import React, { useState } from 'react';
import { KeyboardAvoidingView, StyleSheet, Text, View, TextInput, TouchableOpacity, Keyboard, ScrollView } from 'react-native';
import Task from './component/Task';

export default function App() {
    const [task, setTask] = useState();
    const [taskItems, setTaskItems] = useState([]);
    const handleAddTask = () => {
        Keyboard.dismiss();
        setTaskItems([...taskItems, task])
        setTask("");
    }
    const completeTask = (index) => {
        let itemsCopy = [...taskItems];
        itemsCopy.splice(index, 1);
        setTaskItems(itemsCopy)
    }
    return (
        <View style={styles.container}>
            <TextInput style={styles.text_Field} placeholder={'Write a task'} value={task} onChangeText={text => setTask(text)} />
            <View style={styles.btn_component}>
                <TouchableOpacity onPress={() => handleAddTask()}>
                    <Text style={styles.btn_Add}>Add Task</Text>
                </TouchableOpacity>
            </View>
            <ScrollView style={styles.scroll_component} contentContainerStyle={{ flexGrow: 1 }} keyboardShouldPersistTaps='handled'>
                <View style={styles.items}>
                    {
                        taskItems.map((item, index) => {
                            return (
                                <TouchableOpacity key={index} onPress={() => completeTask(index)}>
                                    <Task text={item} />
                                </TouchableOpacity>
                            )
                        })
                    }
                </View>
            </ScrollView>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        marginTop: 60,
        flex: 1,
        position: 'relative',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    scroll_component: {
        marginTop: 20,
        width: '90%',
    },
    text_Field: {
        paddingVertical: 15,
        paddingHorizontal: 15,
        backgroundColor: '#FFF',
        borderColor: '#000',
        borderWidth: 2,
        borderRadius: 6,
        width: '90%',
    },
    btn_component: {
        marginTop: 20,
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: '#215fdb',
        borderColor: '#000',
        borderWidth: 2,
        borderRadius: 10,
        width: '90%',
    },
    btn_Add: {
        paddingHorizontal: 15,
        backgroundColor: '#215fdb',
        textAlign: 'center',
        color: '#fff',
        fontWeight: 'bold',
    },
});
Elle
ig paste daw ito ha app js
Elle
Elle Zia
amo kasi ito an ak
Elle forwarded a message
Elle Zia
import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { CheckBox, TouchableOpacity } from "react-native-web";

const Task = (props) => {
    return (
        <View style={styles.item}>
            <View style={styles.itemLeft}>
                <View style={styles.circle}></View>
                <Text style={styles.itemText}>{props.text}</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#858383',
        padding: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 10,
        paddingVertical: 50,
    },
    itemLeft: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap',
    },
    circle: {
        width: 27,
        height: 27,
        backgroundColor: '#a73b44',
        borderWidth: 3,
        borderColor: '#fff',
        borderRadius: 60,
        marginRight: 15,
    },
    itemText: {
        maxWidth: '85%',
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
    },
});
export default Task;